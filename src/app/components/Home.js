"use strict";

import React from 'react';

export class Home extends React.Component {
  render() {
    return (
      <div>
        <h3>Home</h3>
      </div>
    );
  }

  // constructor(props) {
  //   super();
  //   this.state = {
  //     age: props.initialAge,
  //     homeLink: props.initialLinkName
  //   };
  // }
  //
  // makeOlder() {
  //   this.setState({
  //     age: this.state.age + 3
  //   });
  // }
  //
  // onChangeLink() {
  //   this.props.changeLink(this.state.homeLink);
  // }
  //
  // onChangeInput(e) {
  //   this.setState({
  //     homeLink: e.target.value
  //   });
  // }
  //
  // render() {
  //   return (
  //     <div>
  //       <p>In a new Component!</p>
  //       <p> Your name is {this.props.name}, your age is {this.state.age}</p>
  //       <hr/>
  //       <button className="btn btn-primary" onClick={() => this.makeOlder()}>Make me older!</button>
  //       <hr/>
  //       <button className="btn btn-primary" onClick={this.props.greet}>Greet</button>
  //       <hr/>
  //       <input type="text" value={this.state.homeLink} onChange={this.onChangeInput.bind(this)}/>
  //       <button className="btn btn-danger" onClick={this.onChangeLink.bind(this)}>Change Header Link</button>
  //     </div>
  //   );
  // }
}

// Home.PropTypes = {
//   name: React.PropTypes.string,
//   initialAge: React.PropTypes.number,
//   greet: React.PropTypes.func,
//   initialLinkName: React.PropTypes.string
// };